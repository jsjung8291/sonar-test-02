import chai = require('chai')
import chaiHttp = require('chai-http')
// const chaiHttp = require('chai-http')
import * as express from "express"
import { test } from '../src/index'
const bodyParser = require('body-parser');

const app = express()
app.use(bodyParser.json());
app.post('/', test)
chai.use(chaiHttp)
const expect = chai.expect

// let abom_test:ABOM_DATA_TEST = { ABOM:{ partnum:'1000', vendor:'TEST'} };


// const abom_data_test:ABOM_DATA =  {} as ABOM_DATA;


describe('*** sonar function ***', () => {
  it('test 1', function (done) {
    // console.log(JSON.stringify(abom_test));
    chai
      .request(app)
      .post('/')
      .send({data : '1'})
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        // console.log(JSON.stringify(res.body));
        done();
      });
  });
  it('test 2', function (done) {
    // console.log(JSON.stringify(abom_test));
    chai
      .request(app)
      .post('/')
      .send({data : '2'})
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        // console.log(JSON.stringify(res.body));
        done();
      });
  });
  it('test 3', function (done) {
    // console.log(JSON.stringify(abom_test));
    chai
      .request(app)
      .post('/')
      .send({data : '3'})
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        // console.log(JSON.stringify(res.body));
        done();
      });
  });
  it('test 4', function (done) {
    // console.log(JSON.stringify(abom_test));
    chai
      .request(app)
      .post('/')
      .send({data : '4'})
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        // console.log(JSON.stringify(res.body));
        done();
      });
  });
});