"use strict";
exports.__esModule = true;
exports.SONAR_TEST = void 0;
var SONAR_TEST = (function () {
    function SONAR_TEST() {
    }
    SONAR_TEST.test = function (data) {
        if (data === '1') {
            console.log('Data is one.');
        }
        else if (data === '2') {
            console.log('Data is two.');
        }
        else if (data === '3') {
            console.log('Data is three.');
        }
        else if (data === '4') {
            console.log('Data is four.');
        }
        else {
            console.log('Data is unknow.');
        }
    };
    return SONAR_TEST;
}());
exports.SONAR_TEST = SONAR_TEST;
