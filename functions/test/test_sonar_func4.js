"use strict";
exports.__esModule = true;
var chai = require("chai");
var chaiHttp = require("chai-http");
var express = require("express");
var index_1 = require("../src/index");
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
app.post('/', index_1.test);
chai.use(chaiHttp);
var expect = chai.expect;
describe('*** sonar function ***', function () {
    it('Get 200 response', function (done) {
        chai
            .request(app)
            .post('/')
            .send({ data: '4' })
            .end(function (err, res) {
            expect(err).to.be["null"];
            expect(res).to.have.status(200);
            done();
        });
    });
});
