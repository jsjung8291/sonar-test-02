import { Request, Response } from 'express'
import {SONAR_TEST} from './SONAR_TEST'





export async function test(req: Request, res: Response) {
  try {

    if (req) {

      if (("body" in req && "data" in req.body) === false) {
        res.status(400);
        res.send('error');
        return;
      }

      console.log('### SERVER RECV ###');
      console.log(req.body);
      SONAR_TEST.test(req.body.data);
      console.log('### SERVER SEND ###');
      res.status(200);
      res.send('error');
    } else {
      res.status(400);
      res.send('error');
    }
  } catch (err) {
    res.status(500);
    res.send(err);
  }
}